# GIRL POWER - Constraint Satisfaction Problem
Constraint Satisfaction Problem is a mathematical problem which contains a set of objects whose state must satisfy certain constraints and limitations. There are three components of CSP: variable, domain, constraints.

## Problem
- [x] Modify a colorless undirected graph into a colored graph in which its nodes that are connected by an edge will have a different color. 
- [x] The user can input how many possible colors they want to be assigned to a node. 
- [x] The program can detect whether it has the solution or not. 
- [x] The user can also see the step-by-step process of backtracking search.

## Members
* 1806173531 - Nabila Edina
* 1806173563 - Tsamara Esperanti E
* 1806173613 - Azzahra Abraara
* 1806241091 - Inez Zahra Nabila