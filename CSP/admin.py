from django.contrib import admin
from .models import Vertex, Color, ColorNbr
# Register your models here.
admin.site.register(Vertex)
admin.site.register(Color)
admin.site.register(ColorNbr)