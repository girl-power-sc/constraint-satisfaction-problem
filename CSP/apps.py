from django.apps import AppConfig


class CspConfig(AppConfig):
    name = 'CSP'
