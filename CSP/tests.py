from django.test import TestCase, Client

# Create your tests here.
class UnitTest(TestCase):
    def test_index_page_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_contains_question(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Input your desired graph:", response_content)


    def test_contains_graph_question(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Input your desired colors:", response_content)

 
