from django.urls import path
from . import views

app_name = 'CSP'

urlpatterns = [
    path('', views.index, name='index'),
]