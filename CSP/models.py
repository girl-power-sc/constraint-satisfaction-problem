from django.db import models

class ColorNbr(models.Model):
    num = models.IntegerField();

class Color(models.Model):
    number_of_colors = models.ForeignKey(ColorNbr, on_delete=models.CASCADE)
    color_name = models.CharField(max_length=10)
    # l = []
    # i = 0;
    # for x in range(int(number_of_colors)):
    #     l.append(color_name)
    # nbr = i;
    
class Vertex(models.Model):
    name = models.CharField(max_length=10)
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    # color_choices = models.Color.color_name
