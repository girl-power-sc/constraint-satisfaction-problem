import java.util.HashMap;
import java.util.Scanner;

public class GraphColoringProblem {
    public static int totalVertices;
    public static int assignedColorArray[];
    public static HashMap<Integer, String> colorMap;
    public static HashMap<Integer, String> verticesGetName;
    public static HashMap<String, Integer> verticesGetNum;

    public static void main(String args[])
    {
        verticesGetName = new HashMap();
        verticesGetNum = new HashMap();
        colorMap = new HashMap<Integer, String>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("How many vertices are there?");
        totalVertices = Integer.parseInt(scanner.nextLine());

        // Initialize two-dimensional array to store edges.
        // Because no edges are added yet, set all values to 0
        int[][] graphArray = new int[totalVertices+1][totalVertices+1];
        for (int i = 0; i < totalVertices; i++) {
            for (int j = 0; j < totalVertices; j++) {
                graphArray[i][j] = 0;
            }
        }

        System.out.println("Name your vertices! (Please press enter after each name)");
        for (int i=1; i < totalVertices+1; i++) {
            String input = scanner.nextLine();
            verticesGetName.put(i, input);
            verticesGetNum.put(input, i);
        }

        System.out.println("How many colors?");
        int totalColors = Integer.parseInt(scanner.nextLine());

        System.out.println("What are the colors? (Please press enter after each color)");
        for (int i=1; i < totalColors+1; i++) {
            String newColor = scanner.nextLine();
            colorMap.put(i, newColor);
        }

        System.out.println("How many Edges?");
        int totalEdges = Integer.parseInt(scanner.nextLine());

        System.out.println("Please enter the edges. Example: A-B where vertex A is connected to vertex B. Don't forget to press enter after each edges");
        for(int i = 0; i < totalEdges; i++) {
            String[] edge = scanner.nextLine().split("-");
            String v1 = edge[0];
            Integer vertexNum1 = verticesGetNum.get(v1);
            String v2 = edge[1];
            Integer vertexNum2 = verticesGetNum.get(v2);

            // If graphArray[i][j] = 1 and graphArray[j][i] = 1
            // There is an edge between vertex i and vertex j also vice versa
            graphArray[vertexNum1][vertexNum2] = 1;
            graphArray[vertexNum2][vertexNum1] = 1;
        }
        scanner.close();

        GraphColoringProblem graph = new GraphColoringProblem();
        graph.graphColoring(graphArray, totalColors);
    }

    // Recursively assign colors to vertices.
    boolean vertexColoring(int graph[][], int numOfColors, int assignedColorArray[], int vertexIndex) {
        if (verticesGetName.get(vertexIndex) == null) {
            System.out.print("");
        } else {
            delay();
            System.out.println();
            System.out.println("Now checking vertex: " + verticesGetName.get(vertexIndex));
        }

        if (vertexIndex == totalVertices+1) {
           delay();
            System.out.println();
            System.out.println("All vertices checked!");
            return true;
        }

        for (int colorCode = 1; colorCode <= numOfColors; colorCode++) {
            if (checkColorIsFine(vertexIndex, graph, assignedColorArray, colorCode)) {
                assignedColorArray[vertexIndex] = colorCode;
                delay();
                System.out.println("Try assigning color: " + colorMap.get(colorCode));
                if (vertexColoring(graph, numOfColors, assignedColorArray, vertexIndex + 1)) {
                    return true;
                }

                assignedColorArray[vertexIndex] = 0;
            }
        }
        delay();
        System.out.println();
        System.out.println("Suitable color not found for vertex " + verticesGetName.get(vertexIndex));
        return false;
    }

    /* Check if the current color assignment is safe for vertex v */
    boolean checkColorIsFine(int vertexIndex, int graph[][], int assignedColorArray[], int colorCode) {
        for (int i = 1; i <= totalVertices; i++) {
            if (graph[vertexIndex][i] == 1 && colorCode == assignedColorArray[i]) {
                delay();
                System.out.println("Vertex " + verticesGetName.get(vertexIndex) + " already has the same color as " + verticesGetName.get(i) + " (" + colorMap.get(colorCode) + ")");
                return false;
            }
        }
        return true;
    }


    /* Backtracking code. Returns false if the m
       colors cannot be assigned, otherwise return true
       and print assignments of colors to all vertices.
        */
    boolean graphColoring(int graph[][], int numOfColors)
    {
        // Initialize all color values as 0.
        delay();
        System.out.println();
        System.out.println("--------------------");
        System.out.println("Backtracking Starts!");
        System.out.println("--------------------");

        assignedColorArray = new int[totalVertices+1];
        for (int i = 0; i < totalVertices+1; i++) {
            assignedColorArray[i] = 0;
        }

        if (!vertexColoring(graph, numOfColors, assignedColorArray, 1))
        {
            delay();
            System.out.println();
            System.out.println("Sorry, the solution doesn't exist.");
            return false;
        }

        printSolution(assignedColorArray);
        return true;
    }

    /* Print Solution */
    void printSolution(int assignedColorArray[])
    {
        System.out.println();
        System.out.println("Solution Exists!");
        for (int i = 1; i < totalVertices+1; i++) {
            delay();
            System.out.println("Color for vertex " + verticesGetName.get(i) + ": "
                    + colorMap.get(assignedColorArray[i]) + " ");
        }
        System.out.println();
    }

    void delay() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
// Credits to Geeks For Geeks